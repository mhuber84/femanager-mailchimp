<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile('femanager_mailchimp', 'Configuration/TsConfig/page.typoscript', 'Femanager to Mailchimp');
