<?php

$feUsersColumns = [
    'tx_femanagermailchimp_newsletter' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:femanager_mailchimp/Resources/Private/Language/locallang.xlf:fe_users.newsletter',
        'config' => [
            'type' => 'check',
            'default' => false,
        ],
    ],
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $feUsersColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'tx_femanagermailchimp_newsletter', '', 'before:tx_femanager_log');
$GLOBALS['TCA']['fe_users']['ctrl']['searchFields'] .= ',' . implode(',', array_keys($feUsersColumns));
