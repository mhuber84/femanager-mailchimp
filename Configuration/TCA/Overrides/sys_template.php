<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('femanager_mailchimp', 'Configuration/TypoScript', 'Femanager to Mailchimp');
