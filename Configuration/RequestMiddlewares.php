<?php

return [
    'frontend' => [
        'mhuber84/femanager-mailchimp/mailchimp-unsubscribe' => [
            'target' => \Mhuber84\FemanagerMailchimp\Middleware\MailchimpUnsubscribe::class,
            'before' => [
                'typo3/cms-frontend/base-redirect-resolver',
            ],
        ],
    ],
];
