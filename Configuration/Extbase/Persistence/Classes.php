<?php

declare(strict_types=1);

return [
    \In2code\Femanager\Domain\Model\User::class => [
        'subclasses' => [
            \Mhuber84\FemanagerMailchimp\Domain\Model\User::class,
        ],
    ],
    \Mhuber84\FemanagerMailchimp\Domain\Model\User::class => [
        'tableName' => 'fe_users',
        'recordType' => '0',
        'properties' => [
            'newsletter' => [
                'fieldName' => 'tx_femanagermailchimp_newsletter',
            ],
        ],
    ],
];
