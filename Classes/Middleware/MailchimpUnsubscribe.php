<?php
namespace Mhuber84\FemanagerMailchimp\Middleware;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\FrontendRestrictionContainer;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MailchimpUnsubscribe implements MiddlewareInterface {

    /**
     * @var ResponseFactoryInterface
     */
    private $responseFactory;

    /**
     * @param ResponseFactoryInterface $responseFactory
     */
    public function __construct(ResponseFactoryInterface $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $extConfig = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('femanager_mailchimp');
        if ($request->getRequestTarget() === '/femanagermailchimp/unsubscribe?secret=' . $extConfig['mailchimp']['webhooksecret']) {
            $data = $request->getParsedBody();
            if(
                $request->getMethod() === 'POST'
                && is_array($data)
                && $data['type'] === 'unsubscribe'
                && $data['data']['list_id'] === $extConfig['mailchimp']['listId']
            ) {
                /** @var QueryBuilder $queryBuilder */
                $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                    ->getQueryBuilderForTable('fe_users');
                $queryBuilder
                    ->setRestrictions(GeneralUtility::makeInstance(FrontendRestrictionContainer::class));
                $queryBuilder
                    ->update('fe_users')
                    ->set('tx_femanagermailchimp_newsletter', 0)->where($queryBuilder->expr()->eq('email', $queryBuilder->createNamedParameter($data['data']['email'])))->executeStatement();

                $response = $this->responseFactory->createResponse()
                    ->withHeader('Content-Type', 'application/json; charset=utf-8');
                $response->getBody()->write(json_encode( ['status' => 'unsubscribe']));
                return $response;
            }
            $response = $this->responseFactory->createResponse()
                ->withHeader('Content-Type', 'application/json; charset=utf-8');
            $response->getBody()->write(json_encode( ['status' => 'hello']));
            return $response;
        }
        return $handler->handle($request);
    }
}
