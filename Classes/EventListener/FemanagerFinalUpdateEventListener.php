<?php

namespace Mhuber84\FemanagerMailchimp\EventListener;

use In2code\Femanager\Event\FinalUpdateEvent;
use In2code\Femanager\Finisher\FinisherRunner;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

class FemanagerFinalUpdateEventListener
{
    /**
     * @var ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * @var ContentObjectRenderer
     */
    protected $contentObject;

    /**
     * @var FinisherRunner
     */
    protected $finisherRunner;

    public function __construct(ConfigurationManagerInterface $configurationManager, ContentObjectRenderer $contentObject, FinisherRunner $finisherRunner)
    {
        $this->configurationManager = $configurationManager;
        $this->contentObject = $contentObject;
        $this->finisherRunner = $finisherRunner;
    }

    public function __invoke(FinalUpdateEvent $event): void
    {
        $settings = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS, 'femanager');
        $this->finisherRunner->callFinishers($event->getUser(), 'updateAction', $settings['edit'], $this->contentObject);
    }
}
