<?php

namespace Mhuber84\FemanagerMailchimp\FemanagerFinisher;

use Mhuber84\FemanagerMailchimp\Domain\Model\Log;
use Mhuber84\FemanagerMailchimp\Domain\Model\User;
use In2code\Femanager\Finisher\AbstractFinisher;
use In2code\Femanager\Utility\LogUtility;
use MailchimpMarketing\ApiClient;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MailchimpFinisher extends AbstractFinisher
{

    /**
     * @var LogUtility
     */
    protected $logUtility;

    /**
     * @param LogUtility $logUtility
     */
    public function initializeFinisher()
    {
        $this->logUtility = GeneralUtility::makeInstance(LogUtility::class);
    }

    public function mailchimpFinisher()
    {
        /** @var User $user */
        $user = $this->getUser();

        $extSettings = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('femanager_mailchimp');
        $mailchimp = new ApiClient();
        $mailchimp->setConfig([
            'apiKey' => $extSettings['mailchimp']['apiKey'],
            'server' => $extSettings['mailchimp']['serverPrefix']
        ]);
        $listId = $extSettings['mailchimp']['listId'];
        $subsciberHash = md5(strtolower($user->getEmail()));

        try {
            $response = $mailchimp->lists->getListMember($listId, $subsciberHash);
        } catch (\Exception $e){
            $response = false;
        }
        if($user->getNewsletter() === true){
            if($response === false || $response->status !== 'subscribed'){
                try {
                    $mergeFields = [];
                    foreach ($this->getConfiguration()['fieldMapping'] as $mailchimpField => $typo3Field) {
                        $mergeFields[$mailchimpField] = $user->_getProperty($typo3Field);
                    }
                    $response = $mailchimp->lists->setListMember($listId, $subsciberHash, [
                        'email_address' => $user->getEmail(),
                        'status_if_new' => 'subscribed',
                        'status' => 'subscribed',
                        'merge_fields' => $mergeFields,
                    ]);
                    $this->logUtility->log(Log::STATUS_MAILCHIMPSUBSCRIBED, $user);
                } catch (\Exception $e){
                    $response = false;
                }
            }
        } else {
            if($response !== false && $response->status !== 'unsubscribed') {
                try {
                    $response = $mailchimp->lists->updateListMember($listId, $subsciberHash, [
                        'status' => 'unsubscribed',
                    ]);
                    $this->logUtility->log(Log::STATUS_MAILCHIMPUNSUBSCRIBED, $user);
                } catch (\Exception $e){
                    $response = false;
                }
            }
        }
    }
}
