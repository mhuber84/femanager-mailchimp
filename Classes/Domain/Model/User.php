<?php

namespace Mhuber84\FemanagerMailchimp\Domain\Model;

class User extends \In2code\Femanager\Domain\Model\User
{

    /**
     * newsletter
     *
     * @var bool
     */
    protected $newsletter;

    /**
     * @return bool
     */
    public function getNewsletter(): bool
    {
        return $this->newsletter;
    }

    /**
     * @param bool $newsletter
     */
    public function setNewsletter(bool $newsletter): void
    {
        $this->newsletter = $newsletter;
    }
}
