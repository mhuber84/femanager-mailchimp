<?php

declare(strict_types=1);
namespace Mhuber84\FemanagerMailchimp\Domain\Model;

/**
 * Class Log
 */
class Log extends \In2code\Femanager\Domain\Model\Log
{
    const STATUS_MAILCHIMPSUBSCRIBED = 276;
    const STATUS_MAILCHIMPUNSUBSCRIBED = 277;
}
