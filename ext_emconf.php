<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Femanager Mailchimp',
    'description' => 'TYPO3 CMS Extension to connect femanager and mailchimp',
    'version' => '3.0.0',
    'category' => 'fe',
    'state' => 'stable',
    'author' => 'Marco Huber',
    'author_email' => 'mail@marco-huber.de',
    'constraints' => [
        'depends' => [
            'typo3' => '12.4.0-12.4.99',
            'femanager' => '8.0.0-8.9.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
