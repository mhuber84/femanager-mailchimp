# TYPO3 Extension "Femanager to Mailchimp"

This extension adds a new field "tx_femanagermailchimp_newsletter" to the fe_users table and integrates it into
the extension [femanager](https://extensions.typo3.org/extension/femanager).

It adds a finisher to the new and edit action to subscribe/unsubscribe the user from a [mailchimp audience/list](https://mailchimp.com/developer/marketing/guides/create-your-first-audience/#add-a-contact-to-an-audience).

It adds a middleware which can be used as [mailchimp webhook](https://mailchimp.com/developer/marketing/guides/sync-audience-data-webhooks/).
If a user unsubscribes directly on mailchimp, the data in TYPO3 is updated.

## Installation

1. Install with `composer require mhuber84/femanager-mailchimp`
**Attention:** If you extend femanager's domain model user in your own theme extension, then add femanager-mailchimp as requirement in your
theme's composer.json instead of your projects composer.json and as depenency in your theme's ext_emconf.php. It has to
be loaded before your theme! In your user domain model extend `\Mhuber84\FemanagerMailchimp\Domain\Model\User` instead
of `\In2code\Femanager\Domain\Model\User` and add `'newsletter' => ['fieldName' => 'tx_femanagermailchimp_newsletter',],`
to the properties in your Configuration/Extbase/Persistence/Classes.php.

2. Include TypoScript
Check if this overwrites your own settings and adjust if necessary! :-)

3. Include Page TsConfig

4. Add Partial `<f:render partial="Fields/Newsletter" arguments="{_all}" />` to your new and edit templates.

5. Configure API in ExtensionManager

6. Configure Webhook "https://example.de/femanagermailchimp/unsubscribe?secret=dummy" in Mailchimp to get updates if a
user unsubscribes. For offline testing use curl:
```
curl -X "POST" "https://example.ddev.site/femanagermailchimp/unsubscribe?secret=dummy" \
-H "Content-Type: application/x-www-form-urlencoded" \
--data-urlencode "type=unsubscribe" \
--data-urlencode "data[email]=test@example.de" \
--data-urlencode "data[list_id]=dummy"
```

## Author

<a href="https://marco-huber.de">Marco Huber</a>, <a href="https://troet.cafe/@mhuber84">@mhuber84@troet.cafe</a>, <a href="mailto:mail@marco-huber.de">mail@marco-huber.de</a>

Feel free to contribute and send in pull requests or create an issue on <a href="https://gitlab.com/mhuber84/femanager-mailchimp">https://gitlab.com/mhuber84/femanager-mailchimp</a>
