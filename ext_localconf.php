<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\In2code\Femanager\Domain\Model\User::class] = [
    'className' => \Mhuber84\FemanagerMailchimp\Domain\Model\User::class,
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\In2code\Femanager\Domain\Repository\UserRepository::class] = [
    'className' => \Mhuber84\FemanagerMailchimp\Domain\Repository\UserRepository::class,
];