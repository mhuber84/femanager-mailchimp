.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. _start:

============
randomizer
============

.. only:: html

    :Classification:
        femanager_mailchimp

    :Version:
        |release|

    :Language:
        en

    :Description:
        TYPO3 CMS Extension to connect femanager and mailchimp

    :Keywords:
        femanager,mailchimp,newsletter

    :Copyright:
        2024

    :Author:
        Marco Huber <https://marco-huber.de>

    :Email:
        mail@marco-huber.de

    :License:
        This document is published under the Open Content License
        available from http://www.opencontent.org/opl.shtml

    :Rendered:
        |today|

    The content of this document is related to TYPO3,
    a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.


Please have a look in the README.md for further documentation.
